<?php

namespace Drupal\block_config_override\Config;

use Drupal\block_config_override\OverrideManager;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Provides language overrides for the configuration factory.
 */
class BlockConfigFactoryOverride implements ConfigFactoryOverrideInterface {

  /**
   * The block configuration override manager.
   *
   * @var \Drupal\block_config_override\OverrideManager
   */
  protected $overrideManager;

  /**
   * Constructs a BlockConfigFactoryOverride object.
   *
   * @param \Drupal\block_config_override\OverrideManager $manager
   *   The block configuration override manager.
   */
  public function __construct(OverrideManager $manager) {
    $this->overrideManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $overrides = [];

    if (!$this->overrideManager::isDisableOverrideEnabled()) {
      foreach ($names as $name) {
        if ($data = $this->overrideManager->get($name)) {
          $overrides[$name]['settings'] = $data;
        }
      }
    }

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'BlockConfigOverride';
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

}
