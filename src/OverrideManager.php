<?php

namespace Drupal\block_config_override;

use Drupal\Component\Utility\DiffArray;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 * Class FieldsHelper.
 */
class OverrideManager {

  /**
   * The Block Config Override collection.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $collection;

  /**
   * MenuLinkContentHelper constructor.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   Key value factory.
   */
  public function __construct(KeyValueFactoryInterface $key_value_factory) {
    $this->collection = $key_value_factory->get('block.config_override.blocks');
  }

  /**
   * Checks whether a block override exists for the passed in config ID.
   *
   * @param string $config_id
   *   The block config ID.
   *
   * @return bool
   *   TRUE if the block config ID has an override attached. FALSE otherwise.
   */
  public function has($config_id) {
    return $this->collection->has($config_id);
  }

  /**
   * Gets the Block's configuration overrides.
   *
   * @param string $config_id
   *   The Block's config ID.
   *
   * @return array
   *   The array of configuration. Defaults to an empty array.
   */
  public function get($config_id) {
    return $this->collection->get($config_id, []);
  }

  /**
   * Clears a Block's configuration overrides.
   *
   * @param string $config_id
   *   The Block's config ID.
   */
  public function clear($config_id) {
    $this->collection->delete($config_id);
  }

  /**
   * Updates a Block's configuration overrides.
   *
   * @param string $config_id
   *   The Block's config ID.
   * @param array $configuration
   *   The configuration to set.
   */
  public function update($config_id, array $configuration) {
    $this->collection->set($config_id, $configuration);
  }

  /**
   * Determine the configuration that changed from the original config.
   *
   * @param array $updated_configuration
   *   The updated configuration.
   * @param array $original_configuration
   *   The original configuration.
   *
   * @return array
   *   An array of the configuration that is different from the original.
   */
  public static function diffConfig(array $updated_configuration, array $original_configuration = []) {
    return DiffArray::diffAssocRecursive($updated_configuration, $original_configuration);
  }

  /**
   * Disable overrides from being applied.
   *
   * @param bool $disable
   *   TRUE if overrides should be disabled. FALSE otherwise.
   */
  public static function disableOverride($disable) {
    $disable_setting = &drupal_static('block_config_override_disable_overrides', NULL);
    $disable_setting = ($disable);
  }

  /**
   * Checks whether overrides are currently disabled from being applied.
   *
   * @return bool
   *   TRUE if overrides are currently disabled. FALSE otherwise.
   */
  public static function isDisableOverrideEnabled() {
    return (drupal_static('block_config_override_disable_overrides', NULL) === TRUE);
  }

  /**
   * Force overrides to be applied.
   *
   * @param bool $forced
   *   TRUE if the override should be forced. FALSE otherwise.
   */
  public static function forceOverride($forced) {
    $force_setting = &drupal_static('block_config_override_force_overrides', NULL);
    $force_setting = ($forced);
  }

  /**
   * Checks whether overrides are currently being forced to be applied.
   *
   * @return bool
   *   TRUE if forced overrides are currently being applied. FALSE otherwise.
   */
  public static function isForceOverrideEnabled() {
    return (drupal_static('block_config_override_force_overrides', NULL) === TRUE);
  }

}
